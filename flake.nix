{
  description = "A Collection Of Nix Utilities";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system;};
      in
    {
      writePythonApplication = {name, contentFile, extraRuntimeInputs ? [], python ? null}: 
          let
            python-to-use = if python == null then pkgs.python311 else python;
            wrappedScript = pkgs.writeText "wrappedScript" (builtins.readFile contentFile);
          in
            pkgs.writeShellApplication {
              name = name;
              runtimeInputs = [
                  wrappedScript
                  python-to-use] ++ extraRuntimeInputs;
              text = ''
                  exec ${python-to-use}/bin/python ${wrappedScript} "$@"
              '';
          };

        writeBabashkaApplication = {name, contentFile, extraRuntimeInputs ? []}: let
            wrappedScript = pkgs.writeText "wrappedScript" (builtins.readFile contentFile);
            in
            pkgs.writeShellApplication {
            name = name;
            runtimeInputs = [
                wrappedScript
                pkgs.babashka] ++ extraRuntimeInputs;
            text = ''
                exec ${pkgs.babashka}/bin/bb ${wrappedScript} "$@"
            '';
            };
        make-mprocs = {name, derivations ? []}: let
            mk-mprocs-procs = ret: derivation: ret ++ [{name = derivation.name;
                                                        value = {cmd = ["${derivation}/bin/${derivation.name}"];};
                                                        }];
            mprocs-procs = builtins.listToAttrs (builtins.foldl' mk-mprocs-procs [] derivations);
            mprocs-config = builtins.toJSON {procs = mprocs-procs;};
            mprocs-config-file = pkgs.writeText "config.yaml" mprocs-config;
          in pkgs.writeShellApplication {
            name = name;
            runtimeInputs = [pkgs.mprocs] ++ derivations;
            text = ''
                  ${pkgs.mprocs}/bin/mprocs --config ${mprocs-config-file}
            '';
        };
    });
}
